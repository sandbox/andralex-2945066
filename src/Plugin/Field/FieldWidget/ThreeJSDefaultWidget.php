<?php
/**
 * @file
 * Contains \Drupal\threejs_field\Plugin\Field\FieldWidget\ThreeJSDefaultWidget.
 */

namespace Drupal\threejs_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'threejs_default' widget.
 *
 * @FieldWidget(
 *   id = "threejs_default",
 *   label = @Translation("Three JS"),
 *   field_types = {
 *     "threejs"
 *   }
 * )
 */
class ThreeJSDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $required = $element['#required'];
    $item =& $items[$delta];

    $element['model'] = [
      '#title' => t('3D model'),
      '#type' => 'managed_file',
      '#required' => $required,
      '#description' => t('Upload a .obj file that contains the 3d-geometry.'),
      '#default_value' => isset($item->model) ? [$item->model] : 0,
      '#upload_location' => 'public://threejs_field/models/',
      '#upload_validators' => [
        'file_validate_extensions' => ['obj'],
      ],
    ];
    $element['texture'] = [
      '#title' => t('Texture'),
      '#type' => 'managed_file',
      '#required' => $required,
      '#description' => t('Upload an image that contains the UV Map'),
      '#default_value' => isset($item->texture) ? [$item->texture] : 0,
      '#upload_location' => 'public://threejs_field/textures/',
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpeg jpg gif'],
      ],
      '#theme' => 'image_widget',
      '#preview_image_style' => 'medium',
    ];

    if (!empty($item->texture)) {
      $fid = $item->texture;
      $file = File::load($fid);

      $file_variables = [
        'style_name' => 'thumbnail',
        'uri' => $file->getFileUri(),
      ];

      // Determine image dimensions.
      $image = \Drupal::service('image.factory')->get($file->getFileUri());
      if ($image->isValid()) {
        $file_variables['width'] = $image->getWidth();
        $file_variables['height'] = $image->getHeight();
      }
      else {
        $file_variables['width'] = $file_variables['height'] = NULL;
      }

      // TODO: Provide better way to display preview.
      $element['preview'] = [
        '#weight' => -10,
        '#theme' => 'image_style',
        '#width' => $file_variables['width'],
        '#height' => $file_variables['height'],
        '#style_name' => $file_variables['style_name'],
        '#uri' => $file_variables['uri'],
      ];
    }

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
        '#type' => 'fieldset',
      );
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      if (is_array($value['model']) && !empty($value['model'])) {
        $value['model'] = reset($value['model']);
      }
      else {
        $value['model'] = NULL;
      }
      if (is_array($value['texture']) && !empty($value['texture'])) {
        $value['texture'] = reset($value['texture']);
      }
      else {
        $value['texture'] = NULL;
      }
    }

    return $values;
  }

}
