<?php
/**
 * @file
 * Contains \Drupal\threejs_field\Plugin\Field\FieldType\ThreeJSField.
 */

namespace Drupal\threejs_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'threejs' field type.
 *
 * @FieldType(
 *   id = "threejs",
 *   label = @Translation("Three JS"),
 *   description = @Translation("This field render 3D objects from .obj file using Three JS library."),
 *   category = @Translation("Reference"),
 *   default_widget = "threejs_default",
 *   default_formatter = "threejs_single",
 * )
 */
class ThreeJSField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'model' => [
          'description' => 'The ID of the model file.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'texture' => [
          'description' => 'The ID of the texture file.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ],
      ],
      'indexes' => [
        'model' => ['model'],
        'texture' => ['texture'],
      ],
      'foreign keys' => [
        'model' => [
          'table' => 'file_managed',
          'columns' => ['model' => 'fid'],
        ],
        'texture' => [
          'table' => 'file_managed',
          'columns' => ['texture' => 'fid'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $model = $this->get('model')->getValue();
    $texture = $this->get('texture')->getValue();
    return empty($model) && empty($texture);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['model'] = DataDefinition::create('integer')
      ->setLabel(t('3D model'))
      ->setDescription(t('The 3D model file id.'))
      ->setSetting('unsigned', TRUE);

    $properties['texture'] = DataDefinition::create('integer')
      ->setLabel(t('Texture'))
      ->setDescription(t('The Texture file id.'))
      ->setSetting('unsigned', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    foreach (['model', 'texture'] as $type) {
      $fid = $this->get($type)->getValue();
      $file = File::load($fid);
      if (!empty($file) && !$file->isPermanent()) {
        $entity = $this->getEntity();
        $file_usage = \Drupal::service('file.usage');
        $file_usage->add($file, 'threejs_field', $entity->getEntityType()->id(), $entity->id());
      }
    }
  }

  // TODO: Implement usage remove.
}
