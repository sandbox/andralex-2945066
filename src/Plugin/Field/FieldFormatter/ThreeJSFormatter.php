<?php
/**
 * @file
 * Contains \Drupal\threejs_field\Plugin\Field\FieldFormatter\ThreeJSFormatter.
 */

namespace Drupal\threejs_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'threejs' formatter.
 *
 * @FieldFormatter(
 *   id = "threejs_single",
 *   label = @Translation("ThreeJS single"),
 *   field_types = {
 *     "threejs"
 *   }
 * )
 */
class ThreeJSFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [
      '#attached' => [
        'library' => ['threejs_field/threejs', 'threejs_field/threejs.field'],
        'drupalSettings' => ['threejsField' => []],
      ],
    ];

    foreach ($items as $delta => $item) {
      $element = [
        'container_id' => 'webGL-container-' . $delta,
      ];

      foreach (['model', 'texture'] as $type) {
        $fid = $item->$type;
        $file = File::load($fid);
        if (empty($file)) {
          break 2;
        }
        $uri = $file->getFileUri();
        $url = Url::fromUri(file_create_url($uri));
        $element['container_id'] .= '-' . $fid;
        $element[$type] = $url->getUri();
      }

      $elements[$delta] = [
        '#theme' => 'threejs_field_formatter',
        '#item' => $element,
      ];

      $elements['#attached']['drupalSettings']['threejsField'][$delta] = $element;
    }

    return $elements;
  }

}
