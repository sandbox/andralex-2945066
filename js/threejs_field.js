var container;
var camera, scene, renderer;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var aspect = window.innerHeight / window.innerWidth;
var objects = [];
var textures = [];
var lights = [];
var radians = 0.01745;

init();
animate();

function init() {
  // HTML work.

  // TODO: Get multiple containers by ID.
  settings = drupalSettings.threejsField[0] || {};
  var containerId = settings.container_id;
  container = document.getElementById(containerId);

  // SETUP.

  // setup - CAMERA.
  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
  camera.position.z = 250;

  // SCENE.
  scene = new THREE.Scene();

  // scene - LIGHTS.
  var ambientLight = new THREE.AmbientLight(0xcccccc, 0.4);
  scene.add(ambientLight);
  var pointLight = new THREE.PointLight(0xffffff, 0.8);
  camera.add(pointLight);
  scene.add(camera);

  // setup - LOAD MANAGER.
  var manager = new THREE.LoadingManager();
  manager.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  // LOAD TEXTURE.
  var textureLoader = new THREE.TextureLoader(manager);
  var texture = textureLoader.load(settings.texture);

  // TODO: load multiple textures into appropriate container.
  // for (var i = 0; i < drupalSettings.threejsField.length; i++) {
  //
  //   var loader = new THREE.ImageLoader(manager);
  //
  //   str_material_uvmap_url = drupalSettings.threejsField[i].material_uvmap;
  //
  //   loader.load(str_material_uvmap_url, (function (url, index) {
  //       return function (image, i) {
  //         textures[index] = new THREE.Texture();
  //         textures[index].image = image;
  //         textures[index].needsUpdate = true;
  //       }
  //     })(str_material_uvmap_url, i)
  //     , onProgress
  //     , onError
  //   );
  // }

  // LOAD OBJECT.

  var onProgress = function (xhr) {
    if (xhr.lengthComputable) {
      var percentComplete = xhr.loaded / xhr.total * 100;
      console.log(Math.round(percentComplete, 2) + '% downloaded');
    }
  };
  var onError = function (xhr) {};

  var loader = new THREE.OBJLoader(manager);
  loader.load(settings.model, function (object) {
    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material.map = texture;
      }
    });
    object.position.y = -95;
    scene.add(object);
  }, onProgress, onError);

  // TODO: load multiple objects into appropriate container.
  // for (var i = 0; i < drupalSettings.threejsField.length; i++) {
  //
  //   str_model_url = drupalSettings.threejsField[i].geometry;
  //
  //   loader.load(str_model_url, (function (url, index) {
  //       return function (object, i) {
  //
  //         objects[index] = object;
  //
  //         // apply texture
  //         objects[index].traverse(function (child) {
  //
  //           if (child instanceof THREE.Mesh) {
  //             child.material.map = textures[index];
  //             child.material.side = THREE.DoubleSide;
  //             console.log(child);
  //           }
  //         });
  //
  //         scene.add(objects[index]);
  //
  //         // apply transform
  //         objects[index].position.x = parseInt(drupalSettings.threejsField[index].position.x);
  //         objects[index].position.y = parseInt(drupalSettings.threejsField[index].position.y);
  //         objects[index].position.z = parseInt(drupalSettings.threejsField[index].position.z);
  //
  //         objects[index].rotation.x = parseInt(drupalSettings.threejsField[index].rotation.x) * radians;
  //         objects[index].rotation.y = parseInt(drupalSettings.threejsField[index].rotation.y) * radians;
  //         objects[index].rotation.z = parseInt(drupalSettings.threejsField[index].rotation.z) * radians;
  //
  //         objects[index].scale.x = parseInt(drupalSettings.threejsField[index].geometry_scale);
  //         objects[index].scale.y = parseInt(drupalSettings.threejsField[index].geometry_scale);
  //         objects[index].scale.z = parseInt(drupalSettings.threejsField[index].geometry_scale);
  //
  //       }
  //     })(str_model_url, i)
  //     , onProgress
  //     , onError
  //   );
  // }

  // setup - RENDERER.
  renderer = new THREE.WebGLRenderer({alpha: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(container.offsetWidth, container.offsetWidth * aspect);
  container.appendChild(renderer.domElement);

  // Event listeners.
  document.addEventListener('mousemove', onDocumentMouseMove, false);
  window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {

  windowHalfX = window.innerWidth / 2;
  windowHalfY = window.innerHeight / 2;

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(container.offsetWidth, container.offsetWidth * aspect);
}

function onDocumentMouseMove(event) {

  mouseX = ( event.clientX - windowHalfX ) / 2;
  mouseY = ( event.clientY - windowHalfY ) / 2;

}

// Animate.
function animate() {
  requestAnimationFrame(animate);
  render();
}

function render() {
  camera.position.x += ( mouseX - camera.position.x ) * .05;
  camera.position.y += ( -mouseY - camera.position.y ) * .05;
  camera.lookAt(scene.position);
  renderer.render(scene, camera);
}