This module adds a field to upload and display 3d objects from OBJ files using the Three.js library.

# Dependencies

- Three.js library:

1\. Download Three.js archive >= 9.1 from https://github.com/mrdoob/three.js/

2\. Extract it as is, rename "mrdoob-three.js-[version]" to "three.js", so the assets are at:

    /libraries/three.js/build/three.min.js

